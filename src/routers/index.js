import { createRouter, createWebHistory } from "vue-router";


const routes = [
  {
    path: '/',
    name: 'index',
    component: () => import('../views/index.vue'),
    meta: {
      title: "首页"
    }
  },
  {
    path: '/play',
    name: 'play',
    component: () => import('../views/play.vue'),
    meta: {
      title: "play"
    }
  }
]


export default createRouter({
  history: createWebHistory(),
  routes,
});